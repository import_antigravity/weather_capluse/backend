0. Ubuntu 18.04

1. В директории app выполнить: 

    `pip3 install -r ./requirements.txt`
2. В директории app/backend/backend создать файл .env со следующим содержимым:

    `DBENGINE=django.db.backends.sqlite3`
3. В директории app/backend выполнить команду: 

    `python3 ./manage.py migrate`
4. В директории app/backend выполнить команду: 

    `python3 ./manage.py loaddata fixtures`
5. В директории app/backend выполнить команду: 

    `APP_CODE=CK4OHVhqvQ6Qn9rHvH9HjCkOGNT3V-QjtBl_hgY5rQM python3 ./manage.py runserver 0.0.0.0:8080`

По всем вопросам Телеграм: @satoshi73