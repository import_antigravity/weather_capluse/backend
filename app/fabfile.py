from fabric import task


@task
def deploy(c):
    with c.cd('/home/ubuntu/weather/docker'):
        c.run('git pull')
        c.run('git submodule init')
        c.run('git submodule update')
        c.run('docker-compose pull && docker-compose down && docker-compose up -d')
        c.run('docker-compose exec -T front-app python3 ./backend/manage.py collectstatic --noinput')
        c.run('docker-compose exec -T front-app python3 ./backend/manage.py migrate')
