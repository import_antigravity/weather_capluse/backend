from django.db import models


class WearType(models.Model):
    POSITIONS = (
        ("head", "Голова"),
        ("neck", "Шея"),
        ("outerwear", "Верхняя одежда"),
        ("body", "Тело"),
        ("legs", "Ноги"),
        ("footwear", "Обувь"),
        ("arms", "Руки"),
        ("face", "Лицо"),
        ("accessory", "Аксессуар"),
    )
    name = models.CharField(max_length=128)
    position = models.CharField(max_length=20, choices=POSITIONS)
    description = models.CharField(max_length=256, blank=True)
    icon = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class WearSet(models.Model):
    STYLES = (
        ('casual', 'Кэжуал'),
        ('classic', 'Классический'),
        ('sport', 'Спортивный стиль'),
        ('glamur', 'Гламур'),
    )
    name = models.CharField(max_length=128)
    style = models.CharField(max_length=10, choices=STYLES, default='casual')
    wear_types = models.ManyToManyField(WearType, related_name='sets')
    max_temp = models.FloatField(default=0)
    image = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.name


class Wear(models.Model):
    name = models.CharField(max_length=128)
    type = models.ForeignKey(
        WearType,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
