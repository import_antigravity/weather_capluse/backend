import os
import json
from datetime import datetime
import dateutil.parser
import requests
import geocoder
import pprint

BASE_WEATHER_URL = 'https://weather.ls.hereapi.com'
APP_ID = os.getenv('APP_ID', 'acG6wu3lEemv7kBZMfFx')
APP_CODE = os.getenv('APP_CODE', 'ffJ9HkssdH3W1I_drrmmhQ')


def make_request(product, lat, lon):
    try:
        result = requests.get(f'{BASE_WEATHER_URL}/weather/1.0/report.json',
                              params={
                                  'product': product,
                                  'latitude': lat,
                                  'longitude': lon,
                                  'oneobservation': 'true',
                                  'language': 'ru-RU',
                                  'apiKey': APP_CODE
                              }).json()
        if product == 'observation':
            return result['observations']['location'][0]['observation']
        elif product == 'forecast_hourly':
            return result['hourlyForecasts']['forecastLocation']['forecast']
        elif product == 'forecast_7days':
            return result['forecasts']['forecastLocation']['forecast']

    except json.JSONDecodeError as e:
        print(e)
        return None

    except KeyError as e:
        print(e)
        return None

    except IndexError as e:
        print(e)
        return None


def find_forecast_by_datetime(forecasts, date_time):
    result = None
    for forecast in forecasts:
        dt = dateutil.parser.parse(forecast['utcTime'])

        if not result and date_time.day == dt.day and dt.hour:
            result = forecast

        if date_time.day == dt.day and date_time.hour == dt.hour:
            result = forecast
    # print(result['utcTime'])
    return result


def forecast_transform(forecast):
    return {
        'temperature': {
            'name': forecast['temperatureDesc'],
            'description': forecast['description']
        },
        'wind': {
            'name': forecast['airDescription'],
            'description': forecast['airDescription'],
            'value': forecast['windSpeed']
        },
        'bright': {
            'name': forecast['skyDescription'],
            'description': forecast['skyDescription']
        }
    }


def get_weather(lat, lon, date_time):
    now = datetime.now()
    print(now, date_time)
    if date_time.day != now.day:
        print('forecast_7days')
        forecast = make_request('forecast_7days', lat, lon)
    else:
        print('forecast_hourly')
        forecast = make_request('forecast_hourly', lat, lon)

    found_forecast = find_forecast_by_datetime(forecast, date_time)
    return forecast_transform(found_forecast), found_forecast['temperature']


BASE_PLACES_URL = 'https://places.ls.hereapi.com'


def get_nearest_places(lat, lon, radius):
    places = requests.get(f'{BASE_PLACES_URL}/places/v1/browse', params={
        'in': f'{lat},{lon};r={radius}',
        'cat': 'eat-drink',
        'language': 'ru-RU',
        'result_types': 'address,place',
        'apiKey': APP_CODE
    }, headers={'Accept-Language': 'ru'}).json()

    result = []
    for item in places['results']['items']:
        name = item['title']
        try:
            name = item['alternativeNames'][0]['name']
        except (KeyError, IndexError):
            pass

        address = ''
        try:
            address = item['vicinity'].replace('<br>', ' ').replace('<br/>', ' ')
        except (KeyError, IndexError):
            pass

        place = {
            'coords': item['position'],
            'name': name,
            'icon_type': item['icon'],
            'type': item['category']['title'],
            'address': address
        }
        result.append(place)
    return result


def get_city_from_request(alice_request):
    try:
        for entity in alice_request['request']['nlu']['entities']:
            if entity['type'] == 'YANDEX.GEO' and 'city' in entity['value']:
                return entity['value']['city']
    except (KeyError, IndexError):
        return None


def get_city_coords(city_name):
    res = geocoder.yandex(city_name)
    if res.ok:
        return res.lat, res.lng
    return None


if __name__ == '__main__':
    # pprint.pprint(requests.get(f'https://weather.ls.hereapi.com/weather/1.0/report.json?apiKey={APP_CODE}&product=observation&name=Berlin').json())
    pprint.pprint(get_weather(45.041338, 41.968378, datetime.now()))
    pprint.pprint(get_weather(45.041338, 41.968378, datetime(2020, 6, 20, 15)))
