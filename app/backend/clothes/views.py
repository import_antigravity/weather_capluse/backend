import json

import dateutil.parser
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from clothes.models import WearSet
from clothes.utils import get_nearest_places, get_weather


@require_POST
@csrf_exempt
def get_info(request):
    data = json.loads(request.body.decode('utf-8'))
    lat = data['lat']
    lon = data['lon']
    date_time = dateutil.parser.parse(data['date_time'])

    forecast, temperature = get_weather(lat, lon, date_time)
    print(date_time, temperature)

    places = get_nearest_places(lat, lon, 1500)

    wear_set, wears_result = get_wear_set_by_temp(temperature)

    return JsonResponse({
        'weather': forecast,
        'places': places,
        'wear': {'parts': wears_result, 'image': wear_set.image}
    })


def get_wear_set_by_temp(temperature):
    wear_set = WearSet.objects.filter(max_temp__gte=temperature).order_by('max_temp')[0]
    wears = wear_set.wear_types.all()
    wears_result = []
    for wear in wears:
        wears_result.append({
            'name': wear.name,
            'type': wear.name,
            'icon': wear.icon,
            'description': wear.description
        })
    return wear_set, wears_result
