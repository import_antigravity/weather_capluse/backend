from django.conf.urls import url

from clothes.views import get_info

urlpatterns = [
    url(r'get_info/', get_info),
]
