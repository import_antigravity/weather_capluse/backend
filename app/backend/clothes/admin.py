from django.contrib import admin

from clothes.models import WearType, WearSet, Wear


@admin.register(WearType)
class WearTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(WearSet)
class WearSetAdmin(admin.ModelAdmin):
    list_display = ('name', 'max_temp', 'image')


@admin.register(Wear)
class WearAdmin(admin.ModelAdmin):
    pass
